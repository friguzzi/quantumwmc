﻿namespace Quantum.MPE {


    open Microsoft.Quantum.Intrinsic;
    open Microsoft.Quantum.Canon;
    open Microsoft.Quantum.Convert;
    open Microsoft.Quantum.Math;
    open Microsoft.Quantum.Oracles;
    open Microsoft.Quantum.Arithmetic;
    open Microsoft.Quantum.Characterization;
    open Microsoft.Quantum.Arrays;
    open Microsoft.Quantum.Measurement;


	operation SprinklerAnc (queryRegister:  Qubit[],  target : Qubit) : Unit is Adj+Ctl 
	{
		using (ancilla=Qubit[3 ])
		{
			X(queryRegister[2]);
			X(ancilla[0]);
			X(ancilla[1]);
			X(ancilla[2]);
		
			CCNOT(queryRegister[0],queryRegister[1],ancilla[0]);
			CCNOT(queryRegister[1],queryRegister[2],ancilla[1]);
			CCNOT(queryRegister[0],queryRegister[2],ancilla[2]);
			(Controlled X)([ancilla[0],ancilla[1],ancilla[2],queryRegister[3],queryRegister[4],queryRegister[5],queryRegister[6]],target);
			CCNOT(queryRegister[0],queryRegister[2],ancilla[2]);
			CCNOT(queryRegister[1],queryRegister[2],ancilla[1]);
			CCNOT(queryRegister[0],queryRegister[1],ancilla[0]);

			X(ancilla[2]);
			X(ancilla[1]);
			X(ancilla[0]);
			X(queryRegister[2]);

		}
	}
	
	operation ApplyMarkingOracleAsPhaseOracle (markingOracle : ((Qubit[], Qubit) => Unit is Adj+Ctl),  register : Qubit[] ) :  Unit is Adj+Ctl 
	{
        
        using (target = Qubit()) 
		{
            // Put the target into the |-⟩ state
            X(target);
            H(target);
                
            // Apply the marking oracle; since the target is in the |-⟩ state,
            // flipping the target if the register satisfies the oracle condition will apply a -1 factor to the state
            markingOracle(register, target);
                
            // Put the target back into |0⟩ so we can return it
            H(target);
            X(target);
        }
	}

    
    // The Grover iteration
    operation GroverIteration (register : Qubit[], oracle : ((Qubit[],Qubit) => Unit is Adj+Ctl)) : Unit is Ctl+Adj
	{
        ApplyMarkingOracleAsPhaseOracle(oracle,register);
        Adjoint PrepareEigenState(register);
        // ApplyToEachCA(H, register);
        
     //           ApplyToEachCA(X, register);
     //   Controlled Z(Most(register), Tail(register));
     //   ApplyToEachCA(X, register);

        // from https://quantumcomputing.stackexchange.com/questions/4268/how-to-construct-the-inversion-about-the-mean-operator/4269#4269
  //      using (ancilla = Qubit()){
   //         (ControlledOnInt(0, X))(register, ancilla); // Bit flips the ancilla to |1⟩ if register is |0...0⟩   
   //         Z(ancilla);                                 // Ancilla phase (and therefore whole register phase) becomes -1 if above condition is satisfied
    //        (ControlledOnInt(0, X))(register, ancilla); // Puts ancilla back in |0⟩  
     //   } 

        ApplyToEachCA(X, register);
        using (ancilla = Qubit()){
    		(Controlled X)([register[0],register[1],register[2],register[3],register[4]],ancilla);
            Z(ancilla);
    		(Controlled X)([register[0],register[1],register[2],register[3],register[4]],ancilla);
}
       ApplyToEachCA(X, register);
        Ry(2.0 * PI(), register[0]);
//        R(PauliI, 2.0 * PI(), register[0]);
        PrepareEigenState(register);
        // ApplyToEachCA(H, register);
    }    

    operation PrepareEigenState(q: Qubit[]): Unit is Ctl+Adj 
    {
            // Prepare the eigenstate of U
            let theta0=2.0*ArcSin(Sqrt(0.55));
			let theta1=2.0*ArcSin(Sqrt(0.3));
			let theta2=2.0*ArcSin(Sqrt(0.7));
			Ry(theta0,q[0]);
			Ry(theta1,q[1]);
			Ry(theta2,q[2]);
//            H(q[0]);
//            H(q[1]);
//            H(q[2]);
            H(q[3]);
            H(q[4]);
            H(q[5]);
            H(q[6]);
	}
	operation QMPE() : Result[] 
	{
        
		using (reg=Qubit[7])
		{
			let oracle = OracleToDiscrete(GroverIteration(_,SprinklerAnc(_,_)));
			// Allocate qubits to hold the eigenstate of U and the phase in a big endian register 
            
            // Prepare the eigenstate of U
    //        $w(s)=0.55$, $w(\neg s)=0.45$, $w(r)=0.3$, $w(\neg r)=0.7$, $w(w)=0.7$ and $w(w)=0.3$
		
            PrepareEigenState(reg);
            for (i in 1 .. 3) {
            GroverIteration(reg, SprinklerAnc);
            }
		    let query= Subarray([0,1,2],reg);
            let state = MultiM(query);

            ResetAll(reg);
            return state;
            }

    }
    operation QMAP() : Result[] 
	{
        
		using (reg=Qubit[7])
		{
			let oracle = OracleToDiscrete(GroverIteration(_,SprinklerAnc(_,_)));
			// Allocate qubits to hold the eigenstate of U and the phase in a big endian register 
            
            // Prepare the eigenstate of U
    //        $w(s)=0.55$, $w(\neg s)=0.45$, $w(r)=0.3$, $w(\neg r)=0.7$, $w(w)=0.7$ and $w(w)=0.3$
		
            let theta0=2.0*ArcCos(Sqrt(0.55));
			let theta1=2.0*ArcCos(Sqrt(0.3));
			let theta2=2.0*ArcCos(Sqrt(0.7));
			Ry(theta0,reg[0]);
			Ry(theta1,reg[1]);
			Ry(theta2,reg[2]);
            H(reg[3]);
            H(reg[4]);
            H(reg[5]);
            H(reg[6]);
            for (i in 1 .. 3) {
            GroverIteration(reg, SprinklerAnc);
            }
		    let query= Subarray([0,2],reg);
            let state = MultiM(query);

            ResetAll(reg);
            return state;
            }

    }
}

